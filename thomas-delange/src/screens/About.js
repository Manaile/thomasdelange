import React from 'react';
import styled from "styled-components";

function About() {
  return (
    <AboutContainer>
      <ImgBloc>
        <img src={require("../assets/img/ThomasD.png")} alt="Thomas Delange maquilleur professionnel" />
        <img src={require("../assets/img/Fond3.png")} alt="tâches d'encres pour sublimer Thomas D" />
      </ImgBloc>
      <TxtBloc>
        <p>Maquilleur professionnel freelance, mes divers expériences m’ont permis de réaliser des maquillages dans des domaines variés tels que la mode, la télévision ou encore l’événementiel.</p>
        <p>Basé en région parisienne, je me déplace en France ainsi qu’à l’étranger pour tous vos projets : publicité, tournages, défilés, mariages, cours individuels et en groupes... Du plus léger au plus créatif, je saurai manier les couleurs, les textures et les formes avec subtilité et minutie.</p>
        <p>J’utilise exclusivement des produits professionnels qui respectent votre peau et l’environnement tout en offrant une tenue irréprochable.</p>
        <p>Je vous invite à vous familiariser avec mon univers en parcourant mon portfolio. N’hésitez pas également à faire un tour sur mes pages Facebook, Instagram et Linkedin.</p>
      </TxtBloc>
    </AboutContainer>
  )
}
const AboutContainer = styled.div`
    display: flex;
    justify-content: space-around;
    text-align: center;
    align-items: center;
    margin: 5em;
    position: relative;
    @media screen and (max-width: 1200px){
      flex-direction: column;
      margin: 5em 1.5em;
    }
`
const ImgBloc = styled.div`
    & img{
      @media screen and (max-width: 600px){
          width: 20em;
        }
    }
  
    & img:last-child{
      animation: zoomIn 2.5s;
      position: absolute;
      width: 55em;
      z-index: -1;
      right: 55%;
      top: -5%;
      @media screen and (min-width: 1800px){
      top: -10%;
      right: 60%;
      }
      @media screen and (min-width: 1200px) and (max-width: 1500px){
        top: 5%;
        right: 30%;
      }
      @media screen and (min-width: 1000px) and (max-width: 1200px){
        right: 0%;
      }
      @media screen and (min-width: 900px) and (max-width: 1000px){
        right: -10%;
      }
      @media screen and (min-width: 600px) and (max-width: 900px){
        right: -10%;
      }
      @media screen and (max-width: 600px){
        width: 40em;
        right: -33%;
      }
    }
`
const TxtBloc = styled.div`
    font-size: 1.4em;
    text-align: left;
    padding-left: 8em;
    @media screen and (max-width: 1200px){
      text-align: center;
      padding: 0%;
    }
`

export default About