import React from 'react';
import styled from "styled-components";

function MentionsLegales() {
  return (
      <ContainerLegal>
        <section>
          <article>
            <h2 className='first'>1 - Édition du site</h2>
            <p>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site internet 'https://manailebenazzi.fr' l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi:</p>
            <p>Propriétaire du site : BENAZZI Manaile <br/>Contact : manaile.benazzi@gmail.com <br/>Adresse : 136 boulevard serurier</p>
            <p>Identification de l'entreprise : BENAZZI Manaile <br/>- Adresse postale : 136 boulevard serurier - </p>
            <p>Hébergeur : Hostinger International Ltd. <br/>- Adresse: 61 Lordou Vironos st. 6023 Larnaca, the Republic of Cyprus -</p>
            <p>Délégué à la protection des données : Benazzi Manaile</p>
          </article>
          <article>
            <h2 className='second'>2 - Propriété intellectuelle et contrefaçons.</h2>
            <p>BENAZZI Manaile est propriétaire des droits de propriété intellectuelle et détient les droits d’usage sur tous les éléments accessibles sur le site internet, notamment les textes, images, graphismes, logos, vidéos, architecture, icônes et sons.</p>
            <p>Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de BENAZZI Manaile.</p>
            <p>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p>
          </article>
          <article>
            <h2 className='third'>3 - Limitations de responsabilité.</h2>
            <p>BENAZZI Manaile ne pourra être tenu pour responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site https://manailebenazzi.fr.</p>
            <p>BENAZZI Manaile décline toute responsabilité quant à l’utilisation qui pourrait être faite des informations et contenus présents sur https://manailebenazzi.fr.</p>
            <p>BENAZZI Manaile s’engage à sécuriser au mieux le site https://manailebenazzi.fr, cependant sa responsabilité ne pourra être mise en cause si des données indésirables sont importées et installées sur son site à son insu.</p>
            <p>Des espaces interactifs (espace contact ou commentaires) sont à la disposition des utilisateurs. BENAZZI Manaile se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données.</p>
            <p>Le cas échéant, BENAZZI Manaile se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie …).</p>
          </article>
          <article>
            <h2 className='fourth'>4 - CNIL et gestion des données personnelles.</h2>
            <p>Conformément aux dispositions de la loi 78-17 du 6 janvier 1978 modifiée, l’utilisateur du site https://manailebenazzi.fr dispose d’un droit d’accès, de modification et de suppression des informations collectées. Pour exercer ce droit, envoyez un message à : <br/>Benazzi Manaile - manaile.benazzi@gmail.com.</p>
            <p>Pour plus d'informations sur la façon dont nous traitons vos données envoyez un message à : <br/>Benazzi Manaile - manaile.benazzi@gmail.com.</p>
          </article>
          <article>
            <h2 className='fifth'>5 - Liens hypertextes et cookies</h2>
            <p>Le site https://manailebenazzi.fr contient des liens hypertextes vers d’autres sites et dégage toute responsabilité à propos de ces liens externes ou des liens créés par d’autres sites vers https://manailebenazzi.fr.</p>
            <p>La navigation sur le site https://manailebenazzi.fr est susceptible de provoquer l’installation de cookie(s) sur l’ordinateur de l’utilisateur.</p>
            <p>Un "cookie" est un fichier de petite taille qui enregistre des informations relatives à la navigation d’un utilisateur sur un site. Les données ainsi obtenues permettent d'obtenir des mesures de fréquentation, par exemple.</p>
            <p>Vous avez la possibilité d’accepter ou de refuser les cookies en modifiant les paramètres de votre navigateur. Aucun cookie ne sera déposé sans votre consentement.</p>
            <p>Les cookies sont enregistrés pour une durée maximale de 12 mois.</p>
            <p>Pour plus d'informations sur la façon dont nous faisons usage des cookies, envoyez un message à: <br/>Benazzi Manaile - manaile.benazzi@gmail.com.</p>
          </article>
          <article>
            <h2 className='sixth'>6 - Droit applicable et attribution de juridiction.</h2>
            <p>Tout litige en relation avec l’utilisation du site https://manailebenazzi.fr est soumis au droit français. En dehors des cas où la loi ne le permet pas, il est fait attribution exclusive de juridiction aux tribunaux compétents.</p>
          </article>
      </section>
      </ContainerLegal>
        
  );
}

const ContainerLegal = styled.div`
    text-align: center;
    padding: 1em;
    & p{
      font-size: 1.5em;
      font-family: Geneva, sans-serif;
    }
    & .first{
      color: #FFB137;
    }
    & .second{
      color: #FF4C27;
    }
    & .third{
      color: #F38FEE;
    }
    & .fourth{
      color: #19D5D2;
    }
    & .fifth{
      color: #05C1F7;
    }
    & .sixth{
      color: #0275FD;
    }
`

export default MentionsLegales