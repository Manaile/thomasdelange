import React, { useState } from 'react';
import styled from "styled-components";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader

import {Carousel} from 'react-responsive-carousel';
import {creations} from '../data/creations.js';

function HomePage() {

  const [current, setCurrent] = useState(0);
  const number = creations.length;

  function nextSlide() {
      setCurrent( current === number - 1 ? 0 : current + 1 );
  }

  function prevSlide() {
      setCurrent( current === 0 ? number - 1 : current - 1 );
  }
      
  return (
    <>
    <Audio>
    <audio 
      controls
      src="https://www.bensound.com/bensound-music/bensound-creativeminds.mp3">
    </audio>
    </Audio>
    {creations.map((slide, index) => {
      return (
        <Container key={slide.id}>
          <Carousel
            showArrows={false}
            showStatus={false}
            showIndicators={false}
            showThumbs={false}
          >
            <div>
              {index === current && <Back src={slide.background} alt={slide.titleBack} className={slide.name} /> }
              {index === current && <Img src={slide.img} alt={slide.titleImg} /> }
              {index === current && <Title>{slide.name}</Title>}
            </div>
          </Carousel>
        </Container>
      );
    })}
    <Buttons>
      <Prev onClick={prevSlide}>prev</Prev>
      <Next onClick={nextSlide}>next</Next>
    </Buttons>

  </>
  )
}

const Container = styled.div`
    position: relative;
`
const Audio = styled.div`
    text-align: center;
    margin-top: 2em;
`
const Title = styled.h2`
    position: absolute;
    text-transform: uppercase;
    animation: slideInDown 2s ;
    top: 75%;
    right: 25%;
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      top: 80%;
      right: 10%;
      }
    @media screen and (max-width: 768px){
      position: initial;
      text-align: center;
    }
`
const Buttons = styled.div`
    @media screen and (max-width: 768px){
      display: flex;
      justify-content: space-around;
      text-align: center;
    }
`

const Prev = styled.button`
    text-transform: uppercase;
    cursor: pointer;
    background: none;
    border: none;
    color: #fff;
    font-size: 2.5em;
    position: absolute;
    top: 115%;
    right: 15%;
    @media screen and (min-width: 2000px){
      top: 85%;
      right: 20%;
    }
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      top: 95%;
      right: 10%;
      }
    @media screen and (max-width: 768px){
      position: initial;
    }
`
const Next = styled.button`
    text-transform: uppercase;
    cursor: pointer;
    background: none;
    border: none;
    color: #fff;
    font-size: 2.5em;
    position: absolute;
    top: 125%;
    right: 10%;
    @media screen and (min-width: 2000px){
      top: 95%;
      right: 15%;
    }
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      top: 105%;
      right: 5%;
      }
    @media screen and (max-width: 768px){
      position: initial;
    }
`
const Img = styled.img`
    padding-top: 7em;
    padding-bottom: 7em;
    max-width: 35em;
    max-height: 50em;
    animation: slideInRight 2s ;
    @media screen and (min-width: 2000px){
      max-width: 35em;
      max-height: 50em;
    }
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      max-width: 25em;
      max-height: 40em;
      }
    @media screen and (max-width: 768px){
      padding-bottom: 1em;
      max-width: 20em;
      max-height: 40em;
    }
`
const Back = styled.img`
    animation: zoomIn 2.5s;
    padding-top: 10em;
    padding-bottom: 10em;
    position: absolute;
    z-index: -1;
    &.Sakuna {
      max-width: 40em;
      max-height: 53em;
      top: 0%;
      right: 8%;
      @media screen and (min-width: 2000px){
        max-height: 50em;
        max-width: 40em;
        top: 5%;
        right: 20%; 
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-height: 40em;
        max-width: 30em;
        top: 5%;
        right: 5%; 
      }
      @media screen and (max-width: 768px){
        max-width: 30em;
        max-height: 35em;
        top: 5%;
        right: 0;
        left: 35%;
      }
    }
    &.Asteria {
      max-width: 45em;
      max-height: 55em;
      top: -10%;
      right: 15%;
      @media screen and (min-width: 2000px){
        max-width: 45em;
        max-height: 55em;
        top: -10%;
        right: 25%;
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 35em;
        max-height: 45em;
        top: -10%;
        right: 10%;
      }
      @media screen and (max-width: 768px){
        max-width: 25em;
        max-height: 45em;
        top: 0;
        right: 0;
        left: 27%;
      }
    }
    &.Shuria {
      max-width: 65em;
      max-height: 70em;
      top: -15%;
      right: 30%;
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-height: 50em;
        top: -15%;
        right: -5%;
      }
      @media screen and (min-width: 500px) and (max-width: 768px){
        max-width: 40em;
        max-height: 50em;
        top: -5%;
        right: 10%;
      }
      @media screen and (max-width: 500px){
        min-width: 30em;
        max-height: 75em;
        top: 5%;
        right: -15%;
      }
    }
    &.Antinéa {
      max-width: 60em;
      max-height: 55em;
      top: 0%;
      right: 25%;
      @media screen and (min-width: 2000px){
        max-width: 55em;
        max-height: 70em;
        top: -15%;
        right: 30%;
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 50em;
        max-height: 45em;
        top: -5%;
        right: 10%;
      }
      @media screen and (max-width: 768px){
        max-width: 35em;
        max-height: 45em;
        top: 0%;
        right: 5%;
      }
    }
    &.Sélénia {
      max-width: 70em;
      max-height: 70em;
      top: -20%;
      right: 30%;
      @media screen and (min-width: 2000px){
        max-width: 70em;
        max-height: 70em;
        top: -15%;
        right: 40%;
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 60em;
        max-height: 60em;
        top: -25%;
        right: 20%;
      }
      @media screen and (min-width: 500px) and (max-width: 768px){
        max-width: 45em;
        top: -25%;
        right: 10%;
      }
      @media screen and (max-width: 500px){
        min-width: 30em;
        top: -5%;
        right: 10%;
      }
    }
    &.Lakshmia {
      max-width: 70em;
      max-height: 80em;
      top: -10%;
      right: 15%;
      @media screen and (min-width: 2000px){
        max-width: 70em;
      max-height: 80em;
      top: -10%;
      right: 25%;
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 50em;
        max-height: 60em;
        top: -10%;
        right: 15%;
      }
      @media screen and (min-width: 500px) and (max-width: 768px){
        max-width: 35em;
        top: 0%;
        right: 10%;
      }
      @media screen and (max-width: 500px){
        min-width: 30em;
        top: 5%;
        right: -10%;
      }
    }
    &.Nehanda {
      max-width: 70em;
      max-height: 90em;
      top: 0%;
      right: 25%;
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 55em;
        max-height: 50em;
        top: -8%;
        right: 20%;
      }
      @media screen and (min-width: 500px) and (max-width: 768px){
        min-width: 30em;
        max-height: 35em;
        top: -5%;
        right: 5%;
      }
      @media screen and (max-width: 500px){
        min-width: 35em;
        max-height: 45em;
        top: -5%;
        right: -10%;
      }
    }
    &.Catrina {
      max-width: 70em;
      max-height: 65em;
      top: -5%;
      right: 30%;
      @media screen and (min-width: 2000px){
        max-width: 70em;
        max-height: 65em;
        top: -5%;
        right: 35%;
     }
      @media screen and (min-width: 768px) and (max-width: 1200px) {
        max-width: 60em;
        max-height: 50em;
        top: -10%;
        right: 25%;
      }
      @media screen and (min-width: 500px) and (max-width: 768px){
        max-width: 40em;
        max-height: 45em;
        top: -10%;
        right: 30%;
      }
      @media screen and (max-width: 500px){
        top: -15%;
        right: 30%;
      }
    }
`
export default HomePage