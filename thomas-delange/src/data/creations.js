
export const creations = [
    {
      "id": 1,
      "background": require("../assets/img/Fond1.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Sakuna.png"),
      "titleImg": "make up inspiré d'une guerrière japonaise",
      "name": "Sakuna"
  },
  {
      "id": 2,
      "background": require("../assets/img/Fond2.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Asteria.png"),
      "titleImg": "make up inspiré de la déesse des étoiles filantes Asteria",
      "name": "Asteria"
    },
    {
      "id": 3,
      "background": require("../assets/img/Fond3.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Shuria.png"),
      "titleImg": "make up inspiré de la déesse du Wakanda Shuri",
      "name": "Shuria"
    },
    {
      "id": 4,
      "background": require("../assets/img/Fond4.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Antinea.png"),
      "titleImg": "make up inspiré de la déesse de la mer Antinéa",
      "name": "Antinéa"
    },
    {
      "id": 5,
      "background": require("../assets/img/Fond5.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Selenia.png"),
      "titleImg": "make up inspiré de la déesse de la lune Séléné",
      "name": "Sélénia"
    },
    {
      "id": 6,
      "background": require("../assets/img/Fond6.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Lakshmia.png"),
      "titleImg": "make up inspiré de la déesse indienne de la fortune Lakshmi",
      "name": "Lakshmia"
    },
    {
      "id": 7,
      "background": require("../assets/img/Fond7.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Nehanda.png"),
      "titleImg": "make up inspiré d'une prêtresse guerrière du Zimbabwe Nehanda",
      "name": "Nehanda"
    },
    {
      "id": 8,
      "background": require("../assets/img/Fond8.png"),
      "titleBack": "tâches d'encres pour sublimer le modèle",
      "img": require("../assets/img/Catrina.png"),
      "titleImg": "make up inspiré d'un personnage populaire de la culture mexicaine de la fête des morts",
      "name": "Catrina"
    }
  ]