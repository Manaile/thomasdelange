import { createGlobalStyle } from 'styled-components';


const KeyFrames = createGlobalStyle`
*,*::before,*::after {
    
    @keyframes slideInDown {
	0% {
	  transform: translate3d(0, -70%, 0);
	  opacity:0
	}
	50%{
	  opacity:0.7
	}
	
	100% {
	  transform: translate3d(0, 0, 0);
	  opacity:1
	}
	}

    @keyframes pulse {
        from {
            transform: scale3d(1, 1, 1);
        }
        
        50% {
            transform: scale3d(1.2, 1.2, 1.2);
        }
        
        to {
            transform: scale3d(1, 1, 1);
        }
    }
     
    @keyframes rubberBand {
        from {
            transform: scale3d(1, 1.8, 1);
        }

        30% {
            transform: scale3d(1.25, 0.75, 1);
        }

        40% {
            transform: scale3d(0.75, 1.55, 1);
        }

        50% {
            transform: scale3d(1.15, 0.85, 1);
        }

        65% {
            transform: scale3d(0.95, 1.85, 1);
        }

        75% {
            transform: scale3d(1.05, 1.05, 1);
        }

        to {
            transform: scale3d(1, 1.8, 1);
        }
    }

    @keyframes slideInRight {
    from {
        transform: translate3d(100%, 0, 0);
        visibility: visible;
    }

    to {
        transform: translate3d(0, 0, 0);
    }
    }

    @keyframes zoomIn {
    from {
        opacity: 0;
        transform: scale3d(0.3, 0.3, 0.3);
    }

    50% {
        opacity: 1;
    }
    }

}
`

export default KeyFrames;

