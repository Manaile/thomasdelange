import { createGlobalStyle } from 'styled-components';


const GlobalStyles = createGlobalStyle`
*,
*:after,
*::before {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

body {
    background-color: #000;
    padding-top: 30vh;
    color: #fff;
}
p{
    font-size: 1.5em;
    font-family: 'Dancing Script', cursive;
}
h1{
    font-family: 'IM Fell DW Pica', serif;
    
    font-size: 3.8em;
}
 h2{
    font-size: 4em;
    font-family: 'IM Fell DW Pica', serif;
}
h3{
    font-size: 3.5em;
    font-family: 'IM Fell DW Pica', serif;

}

`

export default GlobalStyles;