import React, { useState} from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Hamburger from "./Hamburger";

const NavBar = () => {
  
  const[hamburgerOpen, setHamburgerOpen] = useState(false);

  const toggleHamburger = () =>{
    setHamburgerOpen(!hamburgerOpen);
  }

  return (
    <>
        <section>
          <Nav>
            <Link to="about">à propos</Link>
            <Link to="/">portfolio</Link>
          </Nav>
          <div onClick={toggleHamburger}>
            <Hamburger isOpen={hamburgerOpen} />
          </div>
        </section>

      <style>{`
        @media screen and (min-width: 800px) and (max-width: 1200px){
        nav {
          display: ${hamburgerOpen ? 'block' : 'none'};
        }
      }
        @media screen and  (max-width: 800px){
          nav {
            display: ${hamburgerOpen ? 'block' : 'none'};
          }}`
      }
      </style>
    </>
  );
};

const Nav = styled.nav`
  text-align: center;
    a{
      position: relative;
      display: inline-block;
      margin: 15px 25px;
      color: #fff;
      text-transform: uppercase;
      letter-spacing: 1px;
      font-weight: 400;
      font-size: 1.5em;
      padding: 10px;
      font-weight: 700;
    }
    a:hover{
      animation: pulse ease-in-out 1s infinite;
      color: #BE1284;
    }
  @media screen and (min-width: 800px) and (max-width: 1000px){
    animation-name: slideInDown ;
    animation-duration: 2s;
    flex-direction: column;
    width: 100%;
    max-height: 20vh;
  }
  @media screen and  (max-width: 800px){
    animation-name: slideInDown ;
    animation-duration: 2s;
    flex-direction: column;
    width: 100%;
    max-height: 60vh;
  }
  `

export default NavBar;
