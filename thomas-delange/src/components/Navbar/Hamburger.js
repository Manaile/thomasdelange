import React from "react";
import styled from "styled-components";



export default function Hamburger({isOpen}){
    return(
        <>
      <Hamb>
          <Burger className="burger1"></Burger>
          <Burger className="burger2"></Burger>
          <Burger className="burger3"></Burger>
      </Hamb>
      <style jsx>{`
        .burger1{
            transform: ${isOpen ? 'rotate(45deg)' : 'rotate(0)'};
        }
        .burger2{
            transform: ${isOpen ? 'translateX(100%)' : 'translateX(0)'};
            opacity: ${isOpen ? 0 : 1};
        }
        .burger3{
            transform: ${isOpen ? 'rotate(-45deg)' : 'rotate(0)'};
        }
      `}</style>
    </>
    )
}

const Hamb = styled.div`
    width: 2rem;
	height: 2rem;
	justify-content: space-around;
	flex-flow: column nowrap;
	display: none;
    margin-left: 10px;
    padding-top: 10px;
    z-index: 10;
    position: absolute;
    top: 2%;
    right: 2%;
        @media (max-width:1200px){
            display: flex;
            margin-left: 10px;
            padding-top: 10px;
            z-index: 10;
    }
`
const Burger = styled.div `
    width: 1.5rem;
	height: 0.3rem;
	margin: 0.12em;
	border-radius: 10px;
	background-color: white;
	transform-origin: 1px;
	transition: all 0.5s linear;
`