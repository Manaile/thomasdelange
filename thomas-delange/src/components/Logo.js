import styled from "styled-components";
import React from "react";
import logo from "../assets/logo/ThomasD.png"
import { Link } from "react-router-dom";

function Logo() {
  return (
      <Link to="/"><LogoThomas src={logo} alt="logo du makeup artist Thomas DELANGE" /></Link>
  );
}

const LogoThomas = styled.img`
  position: absolute;
	top: 0;
	left: 20;
	width: 18em;
	height: auto;
  @media screen and (min-width: 800px) and (max-width: 1200px) {
    width: 15em;
  }
  @media screen and (max-width: 800px){
    width: 10em;
  }
`;


export default Logo;