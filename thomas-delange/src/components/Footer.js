import React from 'react';
import { Link } from "react-router-dom";
import styled from "styled-components";
import {BsLinkedin, BsFacebook, BsInstagram } from "react-icons/bs";


function Footer() {
  return (
    <Foot>
      <div>
      <Link to="mentionslegales" className='legal'><p>Mentions Légales</p></Link>
        <p>&copy; 2022 - Thomas Delange</p>
      </div>
      <div>
        <a href="https://www.linkedin.com/" target="_blank"><BsLinkedin className="logos"/></a>
        <a href="https://www.facebook.com/" target="_blank"><BsFacebook className="logos"/></a>
        <a href="https://www.instagram.com/" target="_blank"><BsInstagram className="logos"/></a>
      </div>
    </Foot>
  )
}

const Foot = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 3em;
  @media screen and (max-width: 768px){
      flex-direction: column-reverse;
    }
    & div:first-child p{
        margin: 0.5em;
    }
    & div:first-child .legal{
        color: #fff;
        text-decoration: none;
    }
    @media screen and (max-width: 768px){
      display: flex;
      justify-content: center;
      text-align: center;
    }
    & .logos{
      font-size: 1.5em;
      margin: auto 0.8em;
      color: #fff;
      @media screen and (max-width: 768px){
      margin: 1em;
    }
    }
`
export default Footer
