import styled from "styled-components";
import React from "react";
import NavBar from "./Navbar/NavBar";

function Title() {
  return (
    <General>
        <Titleh1>
          <Span1>t</Span1>
          <Span2>h</Span2>
          <Span3>o</Span3>
          <Span4>m</Span4>
          <Span5>a</Span5>
          <Span6>s </Span6>
          <Span7> D</Span7>
          <Span8>e</Span8>
          <Span9>l</Span9>
          <Span10>a</Span10>
          <Span11>n</Span11>
          <Span12>g</Span12>
          <Span13>e</Span13></Titleh1>
        <Titleh2>Make-up artist</Titleh2>
        <NavBar/>
    </General>
  );
}

const General = styled.div`
    text-align: center;
    color: #fff;
    text-transform: uppercase;
`
const Titleh1 = styled.h1`
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      font-size: 3.2em;
    }
    @media screen and (max-width: 768px){
      font-size: 2.5em;
    }
`
const Span1 = styled.span`
    &:hover{
      display: inline-block;
      color: #FFF9B6;
      animation: rubberBand 1s infinite;
    }
`
const Span2 = styled.span`
    &:hover{
      display: inline-block;
      color: #EBCC2D;
      animation: rubberBand 1s infinite;
    }
`
const Span3 = styled.span`
    &:hover{
      display: inline-block;
      color: #BE8B28;
      animation: rubberBand 1s infinite;
    }
`
const Span4 = styled.span`
    &:hover{
      display: inline-block;
      color: #BE5000;
      animation: rubberBand 1s infinite;
    }
`
const Span5 = styled.span`
    &:hover{
      display: inline-block;
      color: #BF1800;
      animation: rubberBand 1s infinite;
    }
`
const Span6 = styled.span`
    &:hover{
      display: inline-block;
      color: #BE327C;
      animation: rubberBand 1s infinite;
    }
`
const Span7 = styled.span`
    &:hover{
      display: inline-block;
      color: #9A4EA7;
      animation: rubberBand 1s infinite;
    }
`
const Span8 = styled.span`
    &:hover{
      display: inline-block;
      color: #715BA1;
      animation: rubberBand 1s infinite;
    }
`
const Span9 = styled.span`
    &:hover{
      display: inline-block;
      color: #1A6F95;
      animation: rubberBand 1s infinite;
    }
`
const Span10 = styled.span`
    &:hover{
      display: inline-block;
      color: #009491;
      animation: rubberBand 1s infinite;
    }
`
const Span11 = styled.span`
    &:hover{
      display: inline-block;
      color: #00A592;
      animation: rubberBand 1s infinite;
    }
`
const Span12 = styled.span`
    &:hover{
      display: inline-block;
      color: #0080B6;
      animation: rubberBand 1s infinite;
    }
`
const Span13 = styled.span`
    &:hover{
      display: inline-block;
      color: #003FBD;
      animation: rubberBand 1s infinite;
    }
`
const Titleh2 = styled.h2`
    font-size: 2em;
    margin-top: -1.2em;
    @media screen and (min-width: 768px) and (max-width: 1200px) {
      font-size: 1.6em;
    }
    @media screen and (max-width: 768px){
      font-size: 1.1em;
    }
`

export default Title;