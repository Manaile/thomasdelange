import 'normalize.css';
import { Routes, Route } from 'react-router-dom';
import React from 'react'

import GlobalStyles from "./styles/GlobalStyles";
import KeyFrames from './styles/KeyFrames';

import Logo from './components/Logo';
import Title from './components/Title';
import Footer from './components/Footer';
import HomePage from './screens/HomePage';
import About from './screens/About';
import MentionsLegales from './screens/MentionsLegales';

function App() {
  return (
    <>
        <GlobalStyles/>
        <KeyFrames/>
        <Logo/>
        <Title/>
        <Routes>
          <Route path="/" element={<HomePage/>} />
          <Route path="/about" element={<About />} />
          <Route path="/mentionslegales" element={<MentionsLegales />} />
        </Routes>
        <Footer/>
    </>
  );
}



export default App;
